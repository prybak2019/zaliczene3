import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service'
import * as io from "socket.io-client"

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})

export class WidgetComponent implements OnInit {
  private socket = io("http://localhost:2000")
  number: string
  chatMessage: string
  validator = /(^[0-9]{9}$)/
  validator2 = /(^$)/
  state: string = "waiting"
  chat :string = ""
  
  constructor(private callService: CallService) { 
	this.callService.getChatMessage().subscribe(chatNew => { 
	  this.chat+=chatNew+",";
	})
  }

  ngOnInit() {
  }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.state = "ringing"
	  this.callService.getCallId().subscribe(id => {
	  this.checkStatus()})
    } else {
      console.info('Numer niepoprawny')
    }
  }
  
  chatSend() {
    if (this.isNonEmpty()) {
		this.socket.emit("chat",this.chatMessage);
    } else {
      console.info('Brak wiadomości')
    }
  }
  
  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }
  
  isNonEmpty(): Boolean {
    return !this.validator2.test(this.chatMessage)
  }
  
  checkStatus() {
	this.callService.getCallStatus().subscribe(stateNew => { this.state = stateNew  })
  }
}
