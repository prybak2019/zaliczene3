# Wymagania - projekt zaliczeniowy
## 1. instrukcja readme 0 - 4
* krok po kroku opisane skąd i jak należy pobrać repozytoria
* jak uruchomić aplikację backendową
* jak uruchomić aplikacje w przeglądarce
* jak uruchomić aplikację pod pm2
* jak uruchomić playbook’a ansiblowego

## 2. aplikacja dzwoni - wpisując numer w aplikacji frontendowej zostanie wykonane połączenie przy użyciu
aplikacji backendowej 0 - 1
## 3. brak haseł/numerów prywatnych w repozytorium oraz jego historii 0 - 1
## 4. wykorzystanie rxjs 0 - 1
## 5. obsługa błędów / problemów w komunikacji 0 - 1
## 6. można zestawić chat pomiędzy dwoma użytkownikami (z wykorzystaniem websocket) 0 - 6
## 7. czytelność kodu (m.in. zbędne komentarze/metody, formatowanie) 0 - 1
## 8. PM2 - konfiguracja oraz logowanie zachowania aplikacji 0 - 5
## 9. Ansible - kompletność playbook’a 0 - 5
## 10. wygląd aplikacji 0 - 1
## Termin oddania projektu zaliczeniowego: 04.06.2019 godzina 23:59

### 1 krok po kroku opisane skąd i jak należy pobrać repozytoria
	Zainstalować gita ze strony sieciowej-należy znaleźć w przeglądarce sieciowej odnośnik na sciągnięcie.
	
	Wejdźmy w repozytorium zaliczeńe3 i odnajdźmy przycisk Clone. Po wciśnięciu otrzymujemy link
	"git clone https://bitbucket.org/prybak2019/zaliczene3.git". Wpiszmy to w lini poleceń w terminalu i wykonajmy.
	
### 1 jak uruchomić aplikację backendową
	Przejdźmy w teczkę backend poleceniem "cd backend" ale najpierw wejdźmy w teczkę zaliczenie3 jaką ściągnęliśmy w poprzednim kroku. 
	Aby wykonać polecenie node app należy uprzednio wstanowić nodeJS. Kolejnie zanim node wystaruje należy
	wykonać polecenie npm install. Potem powinniśmy otzymać wiadomość "app listening on port 3000".
	
### 1 jak uruchomić aplikację w przeglądarce
	W teczce frontend podobnie należy wstanowić frontend poleceniem
	npm install -g @angular/cli. Wykonać również polecenie npm install --save socket.io-client. Aby uruchomić frontend należy
	wykonać ng serve a potem w przegladarce wybrać localhost:4200.	

### 1 jak uruchomić aplikację pod pm2
	Wstanowić pm2 rozkazem npm install pm2 -g.Wykonać rozkaz pm2 reload ecosystem.config.js --env=production. Ale uprzednio wstanowić express rozkazem
	npm install express. Plik ecosystem.config.js jest w teczce backendu.

### 1 jak uruchomić playbook’a ansiblowego
	Pobrać plik inventory oraz plik playbook.yml.Są one w teczce backendu. Wykonać rozkaz na linuxie: ansible-playbook -i inventory --ask-sudo-pass playbook.yml.

### 2 aplikacja dzwoni - wpisując numer w aplikacji frontendowej zostanie wykonane połączenie przy użyciu aplikacji backendowej
	Wpisujemy numer telefonu. Naciskamy przycisk Zadzwoń. W widget\widget.component.ts wykonuje się
	call() i jak numer jest poprawny to z call.service.ts wykonuje się placeCall przy wykorzystaniu http.post na adres
	http://localhost:3000/call z parametrami { number1: '999999999', number2: number }. W odpowiedzi dostajemy data.id co dołączamy
	jako kolejne powiadomienie callId. W backendzie w app.js widzimy że data.id to kolejny numer dla kolejnego połączenia. Widzimy że
	jest w pętli pobierany status połączenia jako interval aż połączenie się nawiąże lub odrzuci i zwracany status przez io zdarzenie
	io.emit("status", status) przy każdej zmianie. Trzeba zauważyć że currentStatus jest jedno dla wszystkich połączeń co może 
	wyzwolić błąd jak wielokrotne połączenia zostaną rozpoczęte naraz.
		W widget\widget.component.ts widzimy dalej że zapisujemy się na odbiór callId więc jak z odpowiedzi otrzymamy data.id to zapisujemy
	się na odbiór statusów. Trzeba zauważyć że jak callId przyjdzie przed naszym zapisem na powiadomienia to nic nie otrzymamy.
	Statusy są w call.service.ts w constructor jako socket.on("status", status => {	this.callStatus.next(status)	})
	czyli są wysyłane kolejne powiadomienia po odbiorze zmiany statusu.
	
### 3 brak numerów prywatnych w repozytorium
	Wykreśliłem swoje numery z repozytorium.

### 4 wykorzystanie rxjs
	Czyli wykorzystanie powiadomień przy odbiorze statusów oraz callId.
	
### 5 obsługa błędów - {success: false}
	Błędy są wysyłane w backendzie jako succes: false albo success: true
	
### 6. można zestawić chat pomiędzy dwoma użytkownikami (z wykorzystaniem websocket) 0 - 6
	W backendzie app.js był io.on("connection", socket => {
	aby odebrać socket dla każdego połączenia. Dodano na czekanie na wiadmość typu chat aby rozesłać każdemu przy pomocy
	socket.on("chat", chat => {
		io.emit("chat", chat)
    })
	We frontendzie call.service.ts dołożono private chatMessage = new Subject<string>() aby rozsyłać powiadomienia o nowych wiadomościach
	dla potencjalnych odbiorców w dokumencie HTML. Udostępniono powiadomienia za pomocą
	getChatMessage() {
    	return this.chatMessage.asObservable()
  	}
	Powiadomienia rozsyłano po dokumencie HTML po otrzymaniu z backendu przy pomocy kodu w CallService w konstruktorze:
	this.socket.on("chat", chat => {
		this.chatMessage.next(chat)
	})
	Pozostało przesłać wiadomość na backend aby ten rozesłał ją każdemu po naciśnięciu przycisku:
	chatSend() {
      if (this.isNonEmpty()) {
	    this.socket.emit("chat",this.chatMessage);
      } else {
        console.info('Brak wiadomości')
      }
	}
	W konstruktorze WidgetComponent podpisano się na odbiór wiadomości:
	this.callService.getChatMessage().subscribe(chatNew => { 
	  this.chat+=chatNew+",";
	})
	,co jest wyświetlane w HTMLu w pliku widget.component.html jako {{chat}} oraz jako tablica dla
	ładniejszego wyglądu
		<table class="table table-stripped table-hover">
				<thead class="thead-default">
					<tr>
						<th>Wiadomość</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="wiadomosc in chat" ng-click="">
											<td>{{wiadomosc}}</td>
					</tr>
				</tbody>
			</table>
### 7. czytelność kodu (m.in. zbędne komentarze/metody, formatowanie) 0 - 1
	Szczególna uwaga została przywiązana na czytelność zapisu.
	
## 8. PM2 - konfiguracja oraz logowanie zachowania aplikacji 0 - 5
	Konfiguracja PM@ jest w pliku ecosystem.config.js. Logowanie zachowania apliakcji następuje
	za każdym wywołaniem console.log("to zachowujemy").
	
### 9. Ansible - kompletność playbook’a 0 - 5
	Playbook został zawarty w pliku playbook.yml i jest on przeznaczony dla linuxa.
	
## 10. wygląd aplikacji 0 - 1
	Szczególny nacisk został nałożony na wygląd aplikacji.